LIBRARY IEEE;
USE ieee.std_logic_1164.all;

ENTITY tlc IS

		PORT (
				clk : 		IN std_logic;
				request : 	IN std_logic;
				reset:		IN std_logic;
				input: 		IN std_logic_vector(2 DOWNTO 0);
				output : 	OUT std_logic
		);
		
END tlc;

ARCHITECTURE tlc_arch OF tlc IS

			-- Build enumerated type for the state machine
			TYPE state_type IS (A, B, C, D, E, F, G, H);
			
			-- Register to hold the current state
			SIGNAL state: state_type;
			
			--To keep track of the number of completed clock cycles
			SIGNAL count: INTEGER := 0;
			
BEGIN

		-- Populates the signal state based on the input
		PROCESS (input, reset)
		BEGIN
				IF request = '0' THEN
						CASE input IS			
								WHEN "000" => state <= A;
								WHEN "001" => state <= B;
								WHEN "010" => state <= C;
								WHEN "011" => state <= D;
								WHEN "100" => state <= E;
								WHEN "101" => state <= F;
								WHEN "110" => state <= G;
								WHEN "111" => state <= H;
						END CASE;
				END IF;
		END PROCESS;
		
		
		-- controls the LED based on the value of 'state'
		PROCESS (clk, reset)
		BEGIN
				IF rising_edge(clk) THEN
						IF reset = '0' THEN 
								count <= 0;
						ELSIF 
							request = '0' THEN
								count <= 0;
						ELSE
								count <= count + 1;
								IF state = A THEN
										IF 	count < 25000000 	THEN output <= '1';
										ELSIF count	< 50000000 	THEN output <= '0';
										ELSIF count < 125000000 THEN output <= '1';
										ELSIF count < 150000000 THEN output <= '0';
										ELSIF count < 350000000 THEN output <= '0';
										ELSE 	count <= 0;
										END IF;
								ELSIF state = B THEN
										IF 	count < 75000000 	THEN output <= '1';
										ELSIF count	< 100000000 THEN output <= '0';
										ELSIF count < 125000000 THEN output <= '1';
										ELSIF count < 150000000 THEN output <= '0';
										ELSIF count < 175000000 THEN output <= '1';
										ELSIF count < 200000000 THEN output <= '0';
										ELSIF count < 225000000 THEN output <= '1';
										ELSIF count < 250000000 THEN output <= '0';
										ELSIF count < 450000000 THEN output <= '0';
										ELSE count <= 0;
										END IF;
								ELSIF state = C THEN
										IF 	count < 75000000 	THEN output <= '1';
										ELSIF count	< 100000000 THEN output <= '0';
										ELSIF count < 125000000 THEN output <= '1';
										ELSIF count < 150000000 THEN output <= '0';
										ELSIF count < 225000000 THEN output <= '1';
										ELSIF count < 250000000 THEN output <= '0';
										ELSIF count < 275000000 THEN output <= '1';
										ELSIF count < 300000000 THEN output <= '0';
										ELSIF count < 500000000 THEN output <= '0';
										ELSE count <= 0;
										END IF;
								ELSIF state = D THEN
										IF 	count < 75000000 	THEN output <= '1';
										ELSIF count	< 100000000 THEN output <= '0';
										ELSIF count < 125000000 THEN output <= '1';
										ELSIF count < 150000000 THEN output <= '0';
										ELSIF count < 175000000 THEN output <= '1';
										ELSIF count < 200000000 THEN output <= '0';
										ELSIF count < 400000000 THEN output <= '0';
										ELSE 	count <= 0;
										END IF;
								ELSIF state = E THEN
										IF 	count < 25000000 	THEN output <= '1';
										ELSIF count	< 50000000 	THEN output <= '0';
										ELSIF count < 250000000 THEN output <= '0';						
										ELSE 	count <= 0;
										END IF;
								ELSIF state = F THEN
										IF 	count < 25000000 	THEN output <= '1';
										ELSIF count	< 50000000 	THEN output <= '0';
										ELSIF count < 75000000 	THEN output <= '1';
										ELSIF count < 100000000 THEN output <= '0';
										ELSIF count < 175000000 THEN output <= '1';
										ELSIF count < 200000000 THEN output <= '0';
										ELSIF count < 225000000 THEN output <= '1';
										ELSIF count < 250000000 THEN output <= '0';
										ELSIF count < 450000000 THEN output <= '0';
										ELSE 	count <= 0;
										END IF;
								ELSIF state = G THEN
										IF 	count < 75000000 	THEN output <= '1';
										ELSIF count	< 100000000 THEN output <= '0';
										ELSIF count < 175000000 THEN output <= '1';
										ELSIF count < 200000000 THEN output <= '0';
										ELSIF count < 225000000 THEN output <= '1';
										ELSIF count < 250000000 THEN output <= '0';
										ELSIF count < 450000000 THEN output <= '0';
										ELSE 	count <= 0;
									END IF;
								ELSIF state = H THEN
										IF 	count < 25000000 	THEN output <= '1';
										ELSIF count	< 50000000 	THEN output <= '0';
										ELSIF count < 75000000 	THEN output <= '1';
										ELSIF count < 100000000 THEN output <= '0';
										ELSIF count < 125000000 THEN output <= '1';
										ELSIF count < 150000000 THEN output <= '0';
										ELSIF count < 175000000 THEN output <= '1';
										ELSIF count < 200000000 THEN output <= '0';
										ELSIF count < 400000000 THEN output <= '0';
										ELSE 	count <= 0;
										END IF;
								END IF;
						ENd IF;
				END IF;
		END PROCESS;
END tlc_arch;