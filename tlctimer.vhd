LIBRARY IEEE;
USE 	ieee.std_logic_1164.all;

ENTITY tlc IS

	PORT(
			clk		:IN	std_logic;
			request	:IN	std_logic;
			reset		:IN	std_logic;
			output	:OUT	std_logic_vector(4 DOWNTO 0);
			timer1 	:OUT std_logic_vector(0 TO 6);
			timer0 	:OUT std_logic_vector(0 TO 6)
	);

END tlc;

ARCHITECTURE tlc_arch OF tlc IS
	
	--Build an enumerated type for the state machine
	TYPE state_type IS (G, Y, R);
	
	--Register to hold the current state
	SIGNAL state : state_type;
	
	SIGNAL count : INTEGER := 0;
	
	BEGIN
	
		--Logic to advance to the next state
		PROCESS (clk, reset)
				VARIABLE request_stat : STD_LOGIC;
			BEGIN
			IF reset = '0' THEN
						state <= G;
						count <= 0; -- Additional line
			ELSIF rising_edge(clk) THEN
					CASE state IS
							WHEN G=>
									IF request = '0' THEN
										request_stat := '0';
									END IF;
									
									IF (request_stat = '0' AND count > 500000000) THEN
											state <= Y;
											count <= 0;
											request_stat := '1';
									ELSE
											count <= count + 1;
									END IF;
									
									--Initial codee with wait time
									--IF (request = '0' AND count > 500000000) THEN
										--state <= Y;
										--count :=0;
									--ELSE
										--count := count + 1;
									--END IF;
									
									--Initial code
									--IF request = '0' THEN
											--state <= Y;
											--count := 0;
									--END IF;
							WHEN Y=>
								--Define time constants
								--(50MHz clk means 50,000,000 cycles/s)
								IF count = 250000000 THEN
										state <= R;
										count <= 0;
								ELSE
										count <= count + 1;
								END IF;
							WHEN R=>
									IF count = 500000000 THEN
											state <= G;
											count <= 0;
									ELSE
											count <= count + 1;
									END IF;
					END CASE;
			END IF;
	END PROCESS;
	
	-- Operates the 7seg displaty to show the countdown based on the value of count
	PROCESS (clk, count)
	BEGIN	
			CASE state IS
					WHEN G =>
							IF 	count = 0 			THEN timer1 <= "1001111"; timer0 <= "0000001";
							ELSIF count < 50000000 	THEN timer1 <= "0000001"; timer0 <= "0001100";
							ELSIF count < 100000000	THEN timer1 <= "0000001"; timer0 <= "0000000";
							ELSIF count < 150000000 THEN timer1 <= "0000001"; timer0 <= "0001111";
							ELSIF count < 200000000 THEN timer1 <= "0000001"; timer0 <= "1100000";
							ELSIF count < 250000000 THEN timer1 <= "0000001"; timer0 <= "0100100";
							ELSIF count < 300000000 THEN timer1 <= "0000001"; timer0 <= "1001100";
							ELSIF count < 350000000 THEN timer1 <= "0000001"; timer0 <= "0000110";
							ELSIF count < 400000000 THEN timer1 <= "0000001"; timer0 <= "0010010";
							ELSIF count < 450000000 THEN timer1 <= "0000001"; timer0 <= "1001111";
							ELSIF count < 500000000 THEN timer1 <= "0000001"; timer0 <= "0000001";
							ELSE timer1 <= "1111110"; timer0 <= "1111110";
							END IF;
					WHEN R =>
							IF 	count = 0 			THEN timer1 <= "1001111"; timer0 <= "0000001";
							ELSIF count < 50000000 	THEN timer1 <= "0000001"; timer0 <= "0001100";
							ELSIF count < 100000000	THEN timer1 <= "0000001"; timer0 <= "0000000";
							ELSIF count < 150000000 THEN timer1 <= "0000001"; timer0 <= "0001111";
							ELSIF count < 200000000 THEN timer1 <= "0000001"; timer0 <= "1100000";
							ELSIF count < 250000000 THEN timer1 <= "0000001"; timer0 <= "0100100";
							ELSIF count < 300000000 THEN timer1 <= "0000001"; timer0 <= "1001100";
							ELSIF count < 350000000 THEN timer1 <= "0000001"; timer0 <= "0000110";
							ELSIF count < 400000000 THEN timer1 <= "0000001"; timer0 <= "0010010";
							ELSIF count < 450000000 THEN timer1 <= "0000001"; timer0 <= "1001111";
							ELSIF count < 500000000 THEN timer1 <= "0000001"; timer0 <= "0000001";
							ELSE timer1 <= "1111110"; timer0 <= "1111110";
							END IF;
					WHEN Y =>
							IF 	count = 0 			THEN timer1 <= "1001111"; timer0 <= "0100100";
							ELSIF count < 50000000 	THEN timer1 <= "0000001"; timer0 <= "1001100";
							ELSIF count < 100000000	THEN timer1 <= "0000001"; timer0 <= "0000110";
							ELSIF count < 150000000 THEN timer1 <= "0000001"; timer0 <= "0010010";
							ELSIF count < 200000000 THEN timer1 <= "0000001"; timer0 <= "1001111";
							ELSIF count < 250000000 THEN timer1 <= "0000001"; timer0 <= "0000001";
							ELSE timer1 <= "1111110"; timer0 <= "1111110";
							END IF;
			END CASE;
	END PROCESS;
	
	-- Output depends solely on the current state
	PROCESS (state)
	BEGIN
			CASE state IS
					WHEN G=>
						output <= "10001";
					WHEN Y=>
						output <= "10010";
					WHEN R=>
						output <= "01100";
			END CASE;
	END PROCESS;
	
						
						
END tlc_arch;